# {{ ansible_managed }}

"{{ item.value.path }}" {
  {% for option in item.value.options | default([]) -%}
  {{ option }}
  {% endfor %}

  {%- if item.value.scripts is defined -%}
  {%- for name, script in item.value.scripts.iteritems() -%}
  {{ name }}
    {{ script }}
  endscript
  {% endfor -%}
  {% endif -%}
}
