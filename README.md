Logrotate
=========

This role's duty is to install and configure Logrotate.

Variables
---------

### `logrotate_scripts`
A dictionary of Logrotate configurations. Each configuration is itself a dictionary with:

- a **required** key `path`, the path where logs to be rotated are placed
- an *optional* key `options`, a list of options for the current configuration
- an *optional* key `scripts`, a dictionary of scripts to be executed on rotation

Example
-------

```yaml
---
logrotate_scripts:
  bedita:
    path: /var/www/bedita/bedita-app/tmp/logs/*.log
    options:
      - weekly
      - missingok
      - minsize 1MB
      - rotate 52
      - compress
      - delaycompress
      - notifempty
      - dateext
```

Credits
-------

This role has been inspired by [nickhammond/ansible-logrotate](https://github.com/nickhammond/ansible-logrotate).
